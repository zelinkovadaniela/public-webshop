package eshop.api.enums;

public enum GenderDto {
    FEMALE, MALE, OTHER
}
