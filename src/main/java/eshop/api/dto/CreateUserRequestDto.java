package eshop.api.dto;

import eshop.api.enums.GenderDto;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Accessors(chain = true)
public class CreateUserRequestDto {

    @NonNull
    private String name;
    @NonNull
    private String lastName;
    @NonNull
    private String email;
    @NonNull
    private String phone;
    @NonNull
    private GenderDto gender;
    @NonNull
    private ZonedDateTime dateOfBirth;
    boolean businessAccount;
    private Long companyIdentificationNumber;
    private String vatIt;
    @NonNull
    private String houseNumber;
    private String orientationNumber;
    @NonNull
    private String city;
    @NonNull
    private String country;
    @NonNull
    private String postalCode;
    @NonNull
    BigDecimal glat;
    @NonNull
    BigDecimal glon;

}
