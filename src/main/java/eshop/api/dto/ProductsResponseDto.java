package eshop.api.dto;

import eshop.dto.Product;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ProductsResponseDto {

    private List<Product> products;
    private long totalElements;
}
