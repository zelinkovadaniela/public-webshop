package eshop.enums;

public enum Gender {
    FEMALE, MALE, OTHER
}
