package eshop.enums;

public enum OrderStatus {
    CREATED, SHIPPED, DELIVERED
}
