package eshop.enums;

public enum PaymentStatus {
    NEW, WAITING, PAYED
}
