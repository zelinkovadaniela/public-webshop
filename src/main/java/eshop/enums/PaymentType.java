package eshop.enums;

public enum PaymentType {
    CASH, CREDIT_CARD
}
