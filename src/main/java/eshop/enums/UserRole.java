package eshop.enums;

public enum UserRole {
    CUSTOMER, ADMIN
}
