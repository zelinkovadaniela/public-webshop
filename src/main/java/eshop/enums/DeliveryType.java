package eshop.enums;

public enum DeliveryType {
    POST, PICK_UP
}
