package eshop.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;

@Data
@Value
@Builder
public class Address {

    @NonNull
    String houseNumber;
    String orientationNumber;
    @NonNull
    String city;
    @NonNull
    String country;
    @NonNull
    String postalCode;
    @NonNull
    BigDecimal glat;
    @NonNull
    BigDecimal glon;

}
