package eshop.dto;

import eshop.api.enums.GenderDto;
import lombok.Data;
import lombok.NonNull;
import lombok.Value;

import java.time.ZonedDateTime;

@Data
@Value
public class User {

    Long id;
    @NonNull
    String name;
    @NonNull
    String lastName;
    @NonNull
    String email;
    @NonNull
    String phone;
    @NonNull
    GenderDto gender;
    @NonNull
    ZonedDateTime dateOfBirth;
    boolean businessAccount;
    Long companyIdentificationNumber;
    String vatIt;
}
