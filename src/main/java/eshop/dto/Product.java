package eshop.dto;

import lombok.Data;
import lombok.NonNull;
import lombok.Value;

@Data
@Value
public class Product {

    Long id;
    @NonNull
    String name;

}
