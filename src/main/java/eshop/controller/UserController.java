package eshop.controller;

import eshop.api.dto.CreateUserRequestDto;
import eshop.controller.exception.UserWithEmailAlreadyExistsException;
import eshop.dto.User;
import eshop.dto.UserWithAddress;
import eshop.mapper.DtoUserRequestMapper;
import eshop.repository.UserRepository;
import eshop.service.user.CreateUserAction;
import eshop.service.user.GetUsersAction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final GetUsersAction getUsersAction;
    private final UserRepository userRepository;
    private final DtoUserRequestMapper dtoUserRequestMapper;
    private final CreateUserAction createUserAction;

    @PostMapping("/users")
    public ResponseEntity<Void> createUser(@RequestBody CreateUserRequestDto userRequest) {
        log.info("REST request to create new User");
        // TODO create exception - UserWithEmailAlreadyExists
        userRepository.findByEmail(userRequest.getEmail()).orElseThrow(UserWithEmailAlreadyExistsException::new);
        final var userWithAddress = dtoUserRequestMapper.map(userRequest);
        createUserAction.create(userWithAddress);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/users")
    public ResponseEntity<Page<User>> getUsers(
            @PageableDefault(direction = Direction.ASC) final Pageable pageable
    ) {
        log.info("REST request to get Users");
        return ResponseEntity.ok(getUsersAction.get(pageable));
    }

}
