package eshop.controller;

import eshop.controller.exception.BadRequestAlertException;
import eshop.dto.Product;
import eshop.service.product.CreateProductAction;
import eshop.service.product.GetProductsAction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ProductController {

    private static final String ENTITY_NAME = "product";

    private final CreateProductAction createProductAction;
    private final GetProductsAction getProductsAction;

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product) throws URISyntaxException {
        log.info("REST request to save Product : {}", product);
        if (product.getId() != null) {
            throw new BadRequestAlertException("A new product cannot be created, product already has an ID", ENTITY_NAME, "idexists");
        }
        Product result = createProductAction.save(product);
        return ResponseEntity.created(new URI("/api/products/" + result.getId()))
//                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @GetMapping("/products")
    public ResponseEntity<Page<Product>> getProducts(
            @PageableDefault(direction = Direction.ASC) final Pageable pageable
    ) {
        log.info("REST request to get Products");
        return ResponseEntity.ok(getProductsAction.get(pageable));
    }

}
