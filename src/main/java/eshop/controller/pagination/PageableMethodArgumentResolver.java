package eshop.controller.pagination;

import eshop.utils.UrlParams.Pagination;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Optional;

public class PageableMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(final MethodParameter parameter) {
        return Pageable.class.equals(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(
            final MethodParameter parameter,
            final ModelAndViewContainer mavContainer,
            final NativeWebRequest webRequest,
            final WebDataBinderFactory binderFactory) {
        final var pageableDefaultOpt = Optional.ofNullable(parameter.getParameterAnnotation(PageableDefault.class));

        final var pageIndex = Optional.ofNullable(webRequest.getParameter(Pagination.PAGE_INDEX))
                .map(Integer::valueOf)
                .orElse(pageableDefaultOpt
                        .map(PageableDefault::page)
                        .orElse(0));

        final var pageSize = Optional.ofNullable(webRequest.getParameter(Pagination.PAGE_SIZE))
                .map(Integer::valueOf)
                .orElse(pageableDefaultOpt
                        .map(PageableDefault::size)
                        .orElse(10));

        final var sortByProperties = Optional.ofNullable(webRequest.getParameter(Pagination.SORT_BY))
                .map(i -> new String[]{i})
                .orElse(pageableDefaultOpt
                        .map(PageableDefault::sort)
                        .orElse(new String[]{}));

        final var sortDirection = Optional.ofNullable(webRequest.getParameter(Pagination.SORT_DIRECTION))
                .map(Direction::valueOf)
                .orElse(pageableDefaultOpt
                        .map(PageableDefault::direction)
                        .orElse(Direction.ASC));

        return PageRequest.of(
                pageIndex,
                pageSize,
                sortByProperties.length == 0 ? Sort.unsorted() : Sort.by(sortDirection, sortByProperties));
    }
}
