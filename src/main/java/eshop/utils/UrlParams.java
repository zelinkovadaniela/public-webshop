package eshop.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UrlParams {

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Pagination {

        public static final String PAGE_SIZE = "pageSize";
        public static final String PAGE_INDEX = "pageIndex";
        public static final String SORT_BY = "sortBy";
        public static final String SORT_DIRECTION = "sortDirection";

    }
}
