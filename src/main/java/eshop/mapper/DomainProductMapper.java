package eshop.mapper;

import eshop.dto.Product;
import eshop.entity.ProductJpa;
import org.mapstruct.Mapper;

@Mapper
public interface DomainProductMapper {

    Product map(final ProductJpa productJpa);
}
