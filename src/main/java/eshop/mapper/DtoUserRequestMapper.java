package eshop.mapper;

import eshop.api.dto.CreateUserRequestDto;
import eshop.dto.UserWithAddress;
import org.mapstruct.Mapper;

@Mapper
public interface DtoUserRequestMapper {

    UserWithAddress map(final CreateUserRequestDto userRequestDto);
}
