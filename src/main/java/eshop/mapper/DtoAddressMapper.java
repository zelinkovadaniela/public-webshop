package eshop.mapper;

import eshop.dto.Address;
import eshop.dto.UserWithAddress;

public class DtoAddressMapper {

    public Address map(
            final UserWithAddress userWithAddress) {
        return Address.builder()
                .houseNumber(userWithAddress.getHouseNumber())
                .orientationNumber(userWithAddress.getOrientationNumber())
                .city(userWithAddress.getCity())
                .country(userWithAddress.getCountry())
                .postalCode(userWithAddress.getPostalCode())
                .build();
    }
}
