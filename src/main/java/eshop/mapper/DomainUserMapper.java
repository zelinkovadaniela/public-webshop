package eshop.mapper;

import eshop.dto.User;
import eshop.dto.UserWithAddress;
import eshop.entity.AddressJpa;
import eshop.entity.UserJpa;
import org.mapstruct.Mapper;

@Mapper
public interface DomainUserMapper {

    User map(final UserJpa userJpa);

    UserJpa map(final UserWithAddress user, final AddressJpa address);
}
