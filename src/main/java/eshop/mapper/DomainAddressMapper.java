package eshop.mapper;

import eshop.dto.Address;
import eshop.entity.AddressJpa;
import org.mapstruct.Mapper;

import java.math.BigDecimal;

@Mapper
public interface DomainAddressMapper {

    AddressJpa map(final Address address);
}
