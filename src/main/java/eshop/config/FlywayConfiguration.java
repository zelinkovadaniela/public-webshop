package eshop.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FlywayConfiguration {

    @Value("${flyway.validate-on-migrate:true}")
    private boolean validateOnMigrate;

    @Bean
    public FlywayConfigurationCustomizer customizer() {
        return configuration -> {
            configuration.baselineOnMigrate(false);
            configuration.outOfOrder(true);
            configuration.validateOnMigrate(validateOnMigrate);
            configuration.table("schema_version");
            configuration.schemas("eshop");
        };
    }
}
