package eshop.service.product;

import eshop.dto.Product;
import eshop.mapper.DomainProductMapper;
import eshop.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class GetProductsAction {

    private final ProductRepository productRepository;
    private final DomainProductMapper domainProductMapper;

    public Page<Product> get(final Pageable pageable) {
        final var pageSize = pageable.getPageSize();
        final var pageNumber = pageable.getPageNumber();
        log.info("Retrieving {} products from DB for page: {}", pageSize, pageNumber);
        final var productsJpa = productRepository.findAllPageable(pageable);

        log.info("Successfully retrieved products from DB: {}", productsJpa);
        final var totalElements = productsJpa.getTotalElements();
        return new PageImpl<>(
                productsJpa.stream().map(domainProductMapper::map).toList(),
                pageable,
                totalElements
        );
    }

}
