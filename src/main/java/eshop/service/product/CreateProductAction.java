package eshop.service.product;

import eshop.dto.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class CreateProductAction {

    @Transactional
    public Product save(Product product) {
        return new Product(product.getId(), product.getName());
    }

}
