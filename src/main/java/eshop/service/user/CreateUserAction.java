package eshop.service.user;

import eshop.dto.UserWithAddress;
import eshop.mapper.DomainUserMapper;
import eshop.mapper.DtoAddressMapper;
import eshop.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CreateUserAction {

    private final CreateUserAddressAction createUserAddressAction;
    private final DtoAddressMapper dtoAddressMapper;
    private final UserRepository userRepository;
    private final DomainUserMapper domainUserMapper;

    @Transactional
    public void create(final UserWithAddress userWithAddress) {
        final var addressJpa = createUserAddressAction.create(dtoAddressMapper.map(userWithAddress));
        userRepository.save(domainUserMapper.map(userWithAddress, addressJpa));
    }
}
