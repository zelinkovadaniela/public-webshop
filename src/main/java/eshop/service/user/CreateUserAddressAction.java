package eshop.service.user;

import eshop.dto.Address;
import eshop.entity.AddressJpa;
import eshop.mapper.DomainAddressMapper;
import eshop.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CreateUserAddressAction {

    private final AddressRepository addressRepository;
    private final DomainAddressMapper domainAddressMapper;

    public AddressJpa create(final Address address) {
        return addressRepository.save(domainAddressMapper.map(address));
    }

}
