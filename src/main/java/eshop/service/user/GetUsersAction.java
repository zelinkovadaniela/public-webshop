package eshop.service.user;

import eshop.dto.User;
import eshop.mapper.DomainUserMapper;
import eshop.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class GetUsersAction {

    private final UserRepository userRepository;
    private final DomainUserMapper domainUserMapper;

    public Page<User> get(final Pageable pageable) {
        final var pageSize = pageable.getPageSize();
        final var pageNumber = pageable.getPageNumber();
        log.info("Retrieving {} users from DB for page: {}", pageSize, pageNumber);
        final var productsJpa = userRepository.findAllPageable(pageable);

        final var totalElements = productsJpa.getTotalElements();
        log.info("Successfully retrieved users from DB: {}", productsJpa);
        return new PageImpl<>(
                productsJpa.stream().map(domainUserMapper::map).toList(),
                pageable,
                totalElements
        );
    }
}
