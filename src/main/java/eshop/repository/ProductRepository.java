package eshop.repository;

import eshop.entity.ProductJpa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductJpa, Long> {

    Page<ProductJpa> findAllPageable(final Pageable pageable);

}
