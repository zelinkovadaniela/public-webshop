package eshop.repository;

import eshop.entity.AddressJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<AddressJpa, Long> {

}
