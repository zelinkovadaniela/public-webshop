create table if not exists product
(
    id                      bigint unsigned not null auto_increment,
    name                    varchar(255)    not null,
    description             text,
    category_id             int unsigned    not null references category(id),
    price                   decimal(10, 2)  not null,
    enabled                 boolean         not null default false,
    created_at              datetime        not null default current_timestamp,
    created_by              bigint unsigned not null,
    updated_at              datetime        not null default current_timestamp on update current_timestamp,
    updated_by              bigint unsigned not null,

    constraint product_pk primary key (id)
);

create table if not exists product_image
(
    id           bigint unsigned not null auto_increment,
    product_id   bigint unsigned not null references product(id),
    path         varchar(255)    not null,
    created_at   datetime        not null default current_timestamp,
    updated_at   datetime        not null default current_timestamp on update current_timestamp,

    constraint product_image_pk primary key (id)
);

create table if not exists category
(
    id                  bigint unsigned         not null auto_increment,
    name                varchar(255)            not null,
    description         text,
    parent_category_id  bigint unsigned references category(id),
    enabled             boolean default false,
    created_at          datetime                not null default current_timestamp,
    created_by          bigint unsigned         not null,
    updated_at          datetime                not null default current_timestamp on update current_timestamp,
    updated_by          bigint unsigned         not null,

    constraint category_pk primary key (id)
);
