create table if not exists address
(
    id                  bigint unsigned not null auto_increment,
    street              varchar(255)    not null,
    house_number        varchar(50)     not null,
    orientation_number  varchar(50),
    city                varchar(255)    not null,
    country             varchar(255)    not null,
    postal_code         varchar(50)     not null,
    glat                decimal(10, 2),
    glon                decimal(10, 2),
    created_at          datetime        not null default current_timestamp,
    updated_at          datetime        not null default current_timestamp on update current_timestamp,

    constraint address_pk primary key (id)
);
