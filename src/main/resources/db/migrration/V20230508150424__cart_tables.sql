create table if not exists cart
(
    id                      bigint unsigned not null auto_increment,
    user_id                 bigint unsigned not null,
    total_price             decimal(10, 2),
    expires_at              datetime,
    created_at              datetime        not null default current_timestamp,
    updated_at              datetime        not null default current_timestamp on update current_timestamp,

    constraint cart_pk primary key (id)
);

create table if not exists cart_item
(
    id           bigint unsigned not null auto_increment,
    product_id   bigint unsigned not null references product(id),
    cart_id      bigint unsigned not null references cart(id),
    created_at   datetime        not null default current_timestamp,
    updated_at   datetime        not null default current_timestamp on update current_timestamp,

    constraint cart_item_pk primary key (id)
);
