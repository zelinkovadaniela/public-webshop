create table if not exists `order`
(
    id                  bigint unsigned not null auto_increment,
    user_id             bigint unsigned not null,
    total_price         decimal(10, 2),
    order_status        enum('CREATED', 'SHIPPED', 'DELIVERED') default 'CREATED',
    address_id          bigint unsigned not null references address(id),
    payment_id          bigint unsigned not null references `payment`(id),
    delivery_type_id    bigint unsigned not null references delivery_type(id),
    finished_at         datetime,
    created_at          datetime                not null default current_timestamp,
    created_by          bigint unsigned         not null,
    updated_at          datetime                not null default current_timestamp on update current_timestamp,
    updated_by          bigint unsigned         not null,

    constraint order_pk primary key (id)
);

create table if not exists order_item
(
    id           bigint unsigned not null auto_increment,
    product_id   bigint unsigned not null references product(id),
    order_id     bigint unsigned not null references `order`(id),
    created_at   datetime        not null default current_timestamp,
    updated_at   datetime        not null default current_timestamp on update current_timestamp,

    constraint order_item_pk primary key (id)
);

create table if not exists payment
(
    id              bigint unsigned not null auto_increment,
    payment_type_id bigint unsigned not null references payment_type(id),
    status          enum('NEW', 'WAITING', 'PAYED') not null default 'NEW',
    created_at      datetime        not null default current_timestamp,
    created_by      bigint unsigned not null,
    updated_at      datetime        not null default current_timestamp on update current_timestamp,
    updated_by      bigint unsigned not null,

    constraint payment_pk primary key (id)
);

create table if not exists payment_type
(
    id     bigint unsigned  not null auto_increment,
    name   varchar(255)     not null,

    constraint payment_type_pk primary key (id)
);

create table if not exists delivery_type
(
    id     bigint unsigned not null auto_increment,
    name   varchar(255)    not null,

    constraint delivery_type_pk primary key (id)
);
