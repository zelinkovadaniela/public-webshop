create table if not exists user
(
    id                              bigint unsigned not null auto_increment,
    name                            varchar(255)    not null,
    last_name                       varchar(255)    not null,
    email                           varchar(255)    not null,
    phone                           varchar(255)    not null,
    address_id                      bigint unsigned not null references address (id),
    delivery_address_id             bigint unsigned references address (id),
    company_identification_number   bigint,
    vat_id                          varchar(50),
    date_of_birth                   varchar(50)     not null,
    gender                          enum ('FEMALE', 'MALE', 'OTHER') not null,
    user_role                       enum ('CUSTOMER', 'ADMIN') not null default 'CUSTOMER',
    orders_count                    int,
    deleted                         boolean default false,
    created_at                      datetime        not null default current_timestamp,
    updated_at                      datetime        not null default current_timestamp on update current_timestamp,

    constraint user_pk primary key (id)
);
